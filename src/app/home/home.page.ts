import { Component } from "@angular/core";
import { HomeService } from "../services/home.service";
import { Home } from "../models/home.model";
@Component({
  selector: "app-home",
  templateUrl: "home.page.html",
  styleUrls: ["home.page.scss"]
})
export class HomePage {
  data: any;
  home: Home = new Home();

  constructor(private homeService: HomeService) {}

  ngOnInit() {
    this.homeService.getData().subscribe(
      Data => {
        // const returnedData = Data.json()
        const returnedData = JSON.parse(Data.toString());
        this.data = returnedData;
        this.home.img = returnedData.img;
        this.home.title = returnedData.title;
        this.home.address = returnedData.address;
        this.home.date = returnedData.date;
        this.home.interest = returnedData.interest;
        this.home.isLiked = returnedData.isLiked;
        this.home.latitude = Math.round(returnedData.latitude) ;
        this.home.longitude = Math.round(returnedData.longitude);
        this.home.occasionDetail = returnedData.occasionDetail;
        this.home.price = returnedData.price;
        this.home.reservTypes = returnedData.reservTypes;
        this.home.trainerImg = returnedData.trainerImg;
        this.home.trainerInfo = returnedData.trainerInfo;
        this.home.trainerName = returnedData.trainerName;
        console.log(returnedData);
      },
      error => {
        console.error("An error occurred in getting data ", error);
      }
    );
  }
}
