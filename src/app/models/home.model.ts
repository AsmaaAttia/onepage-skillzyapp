export class Home {
  img: [] = [];
  title: string = "";
  interest: string = "";
  price: any;
  address: string = "";
  date: string = "";
  isLiked: boolean;
  latitude: any;
  longitude: any;
  occasionDetail: string = "";
  trainerName: string = "";
  trainerImg: string = "";
  trainerInfo: string = "";
  reservTypes: [
    {
      id: any;
      name: string;
      count: number;
      price: number;
    }
  ];
}
