import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class HomeService {
  API_URL = 'https://skillzycp.com/api/UserApi/getOneOccasion/389/0';

  constructor( private http: HttpClient ) { }
  getData() {
    return this.http.get(this.API_URL);
  }
}